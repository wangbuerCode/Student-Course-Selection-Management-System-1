package dao;

public class sUser {
       private Integer s_id;
       private String s_word;
       private String s_pass;
       private String s_name;
       private Integer s_bj;
       private String s_xy;
       private Integer s_xk;
       
	public Integer getS_xk() {
		return s_xk;
	}
	public void setS_xk(Integer s_xk) {
		this.s_xk = s_xk;
	}
	public Integer getS_id() {
		return s_id;
	}
	public void setS_id(Integer s_id) {
		this.s_id = s_id;
	}
	public String getS_name() {
		return s_name;
	}
	public void setS_name(String s_name) {
		this.s_name = s_name;
	}

	public String getS_word() {
		return s_word;
	}
	public void setS_word(String s_word) {
		this.s_word = s_word;
	}
	public String getS_pass() {
		return s_pass;
	}
	public void setS_pass(String s_pass) {
		this.s_pass = s_pass;
	}
	public Integer getS_bj() {
		return s_bj;
	}
	public void setS_bj(Integer s_bj) {
		this.s_bj = s_bj;
	}
	public String getS_xy() {
		return s_xy;
	}
	public void setS_xy(String s_xy) {
		this.s_xy = s_xy;
	}
       
       
}
