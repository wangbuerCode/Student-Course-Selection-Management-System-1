package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.User;
import dao.userDao;
import dao.kUser;

public class admin extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public admin() {
		super();
	}


	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}


	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
         doPost(request, response);
	}


	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		      request.setCharacterEncoding("utf-8");
		      String zsgc = request.getParameter("zsgc").replace(" ", "");
		      if(zsgc.equals("dl")){
		    	  
                     dlAdmin(request, response);
                     
		      }else if(zsgc.equals("zj")){
		    	  
		    	  addkUser(request, response);
		    	  allkUser(request, response);
		    	  
		      }else if(zsgc.equals("xg")){
		    	  
		    	  updatekUser(request, response);
		    	  allkUser(request, response);
		    	  
		      }else if(zsgc.equals("sc")){
		    	 
		    	  delectkUser(request, response);
		    	  allkUser(request, response);
		    	  
		      }else{
		    	  
		    	  request.getRequestDispatcher("error.jsp").forward(request, response);
		    	  
		      }
		      

	}


	public void init() throws ServletException {
		// Put your code here
	}
	
	private void dlAdmin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
  	  String name = request.getParameter("word").replace(" ", "");
	      String pass = request.getParameter("pass");
	      User user = new User();
	      user.setU_word(name);
	      user.setU_pass(pass);
	      if(new userDao().adminUser(user)){
	    	  request.getSession().setAttribute("gly", name);
	    	  allkUser(request, response);
	      }else{
	    	  request.getRequestDispatcher("admin.jsp").forward(request, response);
	      }
	}
	private void addkUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  String kcm = request.getParameter("kcm").replace(" ", "");
    	  String js  = request.getParameter("js");
    	  String jxl = request.getParameter("jxl");
    	  String sj  =request.getParameter("sj");
    	  String xf = request.getParameter("xf");
    	  kUser kuser = new kUser();
    	  kuser.setK_kcm(kcm);
    	  kuser.setK_js(js);
    	  kuser.setK_jxl(Integer.parseInt(jxl));
    	  kuser.setK_sj(sj);
    	  kuser.setK_xf(Integer.parseInt(xf));
          new userDao().addkUser(kuser);

	}
	
	private void updatekUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  String id  =request.getParameter("id");
		  String kcm = request.getParameter("kcm").replace(" ", "");
		  String js  = request.getParameter("js");
		  String jxl = request.getParameter("jxl");
		  String sj  =request.getParameter("sj");
		  String xf = request.getParameter("xf");
		  //System.out.println(id+"---"+kcm+"---"+js+"---"+jxl+"---"+sj+"---"+xf);
		  kUser kuser = new kUser();
		  kuser.setK_id(Integer.parseInt(id));
		  kuser.setK_kcm(kcm);
		  kuser.setK_js(js);
		  kuser.setK_jxl(Integer.parseInt(jxl));
		  kuser.setK_sj(sj);
		  kuser.setK_xf(Integer.parseInt(xf));
         new userDao().updatekUser(kuser);   
	}
	
	private void delectkUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  String id  =request.getParameter("id");
	   	  kUser kuser = new kUser();
	   	  kuser.setK_id(Integer.parseInt(id));
         new userDao().deletekUser(kuser);  
	}
	private void allkUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		List<kUser> kuser=new userDao().getAllkUser();
		request.getSession().setAttribute("bgkUser", kuser);
		request.getRequestDispatcher("/background.jsp").forward(request, response);   
	}

}
