package dao;

public class User {
	private Integer id;
	private String u_word;
	private String u_pass;
	private String u_name;
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getU_word() {
		return u_word;
	}

	public void setU_word(String u_word) {
		this.u_word = u_word;
	}

	public String getU_pass() {
		return u_pass;
	}

	public void setU_pass(String u_pass) {
		this.u_pass = u_pass;
	}

	public String getU_name() {
		return u_name;
	}

	public void setU_name(String u_name) {
		this.u_name = u_name;
	}


}
