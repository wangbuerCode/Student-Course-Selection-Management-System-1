package dao;

public class kUser {
       private Integer k_id;
       private String k_kcm;
       private String k_js;
       private Integer k_jxl;
       private String k_sj;
       private Integer k_xf;
	public Integer getK_id() {
		return k_id;
	}
	public void setK_id(Integer k_id) {
		this.k_id = k_id;
	}
	public String getK_kcm() {
		return k_kcm;
	}
	public void setK_kcm(String k_kcm) {
		this.k_kcm = k_kcm;
	}
	public String getK_js() {
		return k_js;
	}
	public void setK_js(String k_js) {
		this.k_js = k_js;
	}
	public Integer getK_jxl() {
		return k_jxl;
	}
	public void setK_jxl(Integer k_jxl) {
		this.k_jxl = k_jxl;
	}
	public String getK_sj() {
		return k_sj;
	}
	public void setK_sj(String k_sj) {
		this.k_sj = k_sj;
	}
	public Integer getK_xf() {
		return k_xf;
	}
	public void setK_xf(Integer k_xf) {
		this.k_xf = k_xf;
	}
       
}
