package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.kUser;

public class demo extends HttpServlet {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public demo() {
		super();
	}

	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}


	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
               doPost(request, response);
	}


	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
                request.getSession().setAttribute("yh", null);
                request.getSession().setAttribute("gly", null);
                List<kUser> kusers = new ArrayList<kUser>();
                request.getSession().setAttribute("kuse", kusers);
                request.getSession().setAttribute("kuser1", new kUser());
                request.getRequestDispatcher("index.jsp").forward(request, response);
	}


	public void init() throws ServletException {
		// Put your code here
	}

}
