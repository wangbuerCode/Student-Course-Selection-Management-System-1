package dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class userDao{
	private Connection con = null;
	private PreparedStatement ps = null;
	static {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void prepareConnection() {
		try {
			if (con == null || con.isClosed()) {
				con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/student?characterEncoding=utf8&useSSL=false&serverTimezone=UTC&rewriteBatchedStatements=true", "root", "123456");
			}
		} catch (SQLException e) {
			throw new RuntimeException("###" + e.getMessage());
		}
	}

	private void close() {
		try {
			if (ps != null) {
				ps.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException("?????????:" + e.getMessage());
		}
	}

	private void rollback() {
		try {
			con.rollback();
		} catch (SQLException e) {
			throw new RuntimeException("??????:" + e.getMessage());
		}
	}
  
	public boolean yzUser(sUser suser){
		boolean val = false;

		try {
			prepareConnection();
			ps = con.prepareStatement("select * from s_user ");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				if(rs.getString(2).equals(suser.getS_word()) &&  rs.getString(3).equals(suser.getS_pass())){
					 val = true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return val;
		
	}
	
	public int zcUser(sUser suser){
		int i =0;

		try {
			prepareConnection();
			con.setAutoCommit(false);
			String sql = "insert into s_user (s_word,s_pass,s_name,s_bj,s_xy) values(?,?,?,?,?)";
			ps = con.prepareStatement(sql);
			ps.setString(1, suser.getS_word());
			ps.setString(2, suser.getS_pass());
			ps.setString(3, suser.getS_name());
			ps.setInt(4, suser.getS_bj());
			ps.setString(5, suser.getS_xy());
			i = ps.executeUpdate();
			con.commit();
		} catch (SQLException e) {
			rollback();
			e.printStackTrace();
		}finally{
			close();
		}
		return i;
	}
	
	public List<kUser> getAllkUser() {
		List<kUser> kusers = new ArrayList<kUser>();
		try {
			prepareConnection();
			ps = con.prepareStatement("select * from k_user ");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				kUser kuser = new kUser();
				kuser.setK_id(rs.getInt(1));
				kuser.setK_kcm(rs.getString(2));
				kuser.setK_js(rs.getString(3));
				kuser.setK_jxl(rs.getInt(4));
				kuser.setK_sj(rs.getString(5));
				kuser.setK_xf(rs.getInt(6));
				kusers.add(kuser);
				// ?????????к??м???????????????????User????????????List??
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return kusers;
	}
	
	public kUser getkUserById(Integer id){
		kUser kuser = null;
		try {
			prepareConnection();
			ps = con.prepareStatement("select * from k_user where k_id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				kuser = new kUser();
				kuser.setK_id(rs.getInt(1));
				kuser.setK_kcm(rs.getString(2));
				kuser.setK_js(rs.getString(3));
				kuser.setK_jxl(rs.getInt(4));
				kuser.setK_sj(rs.getString(5));
				kuser.setK_xf(rs.getInt(6));
				
				// ?????????к??м???????????????????User????
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return kuser;
	}
	
	public Integer getNameByid(sUser suser){
		Integer a = null;
		try {
			prepareConnection();
			ps = con.prepareStatement("select * from s_user where s_word=?");
			ps.setString(1, suser.getS_word());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				a = rs.getInt(7);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return a;
	}
	
	public kUser getIdByKc(Integer a){
		kUser kuser = null;
		try {
			prepareConnection();
			ps = con.prepareStatement("select * from k_user where k_id=?");
			ps.setInt(1, a);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				kuser = new kUser();
				kuser.setK_id(rs.getInt(1));
				kuser.setK_kcm(rs.getString(2));
				kuser.setK_js(rs.getString(3));
				kuser.setK_jxl(rs.getInt(4));
				kuser.setK_sj(rs.getString(5));
				kuser.setK_xf(rs.getInt(6));
				
				// ?????????к??м???????????????????User????
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return kuser;
	}
	
	
	
	public kUser getkUserByName(String name){
		kUser kuser = null;
		try {
			prepareConnection();
			ps = con.prepareStatement("select * from k_user where k_kcm=?");
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				kuser = new kUser();
				kuser.setK_id(rs.getInt(1));
				kuser.setK_kcm(rs.getString(2));
				kuser.setK_js(rs.getString(3));
				kuser.setK_jxl(rs.getInt(4));
				kuser.setK_sj(rs.getString(5));
				kuser.setK_xf(rs.getInt(6));
				
				// ?????????к??м???????????????????User????
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return kuser;
	}
	
	
	public int bcId(sUser suser) {
	int i = 0;
	try {
		prepareConnection();
		con.setAutoCommit(false);
		ps = con
				.prepareStatement("update s_user set s_xk=? where s_word=?");
        ps.setInt(1, suser.getS_xk());
        ps.setString(2, suser.getS_word());
		i = ps.executeUpdate();
		con.commit();
	} catch (SQLException e) {
		rollback();
		e.printStackTrace();
	} finally {
		close();
	}
	return i;
}
	
	public boolean adminUser(User user){
		boolean val = false;

		try {
			prepareConnection();
			ps = con.prepareStatement("select * from admin ");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				if(rs.getString(2).equals(user.getU_word()) &&  rs.getString(3).equals(user.getU_pass())){
					 val = true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return val;
		
	}
	
	public int addkUser(kUser kuser) {
		int i = 0;
		try {
			prepareConnection();
			con.setAutoCommit(false);
			ps = con.prepareStatement("insert into k_user (k_kcm,k_js,k_jxl,k_sj,k_xf) values(?,?,?,?,?)");
			ps.setString(1, kuser.getK_kcm());
			ps.setString(2, kuser.getK_js());
			ps.setInt(3, kuser.getK_jxl());
			ps.setString(4, kuser.getK_sj());
			ps.setInt(5, kuser.getK_xf());
			i = ps.executeUpdate();
			con.commit();
		} catch (SQLException e) {
			rollback();
			e.printStackTrace();
		} finally {
			close();
		}
		return i;
	}

	public int deletekUser(kUser kuser) {
		int i = 0;
		try {
			prepareConnection();
			con.setAutoCommit(false);
			ps = con.prepareStatement("delete from k_user where k_id=?");
			ps.setInt(1, kuser.getK_id());
			i = ps.executeUpdate();
			con.commit();
		} catch (SQLException e) {
			rollback();
			e.printStackTrace();
		} finally {
			close();
		}
		return i;
	}

	public int updatekUser(kUser kuser) {
		int i = 0;
		try {
			prepareConnection();
			con.setAutoCommit(false);
			ps = con.prepareStatement("update k_user set k_kcm=?,k_js=?,k_jxl=?,k_sj=?,k_xf=?  where k_id=?");
			ps.setString(1, kuser.getK_kcm());
			ps.setString(2, kuser.getK_js());
			ps.setInt(3, kuser.getK_jxl());
			ps.setString(4, kuser.getK_sj());
			ps.setInt(5, kuser.getK_xf());
			ps.setInt(6, kuser.getK_id());
			i = ps.executeUpdate();
			con.commit();
		} catch (SQLException e) {
			rollback();
			e.printStackTrace();
		} finally {
			close();
		}
		return i;
	}

//	public List<User> getAllUsers() {
//		List<User> users = new ArrayList<User>();
//		try {
//			prepareConnection();
//			ps = con.prepareStatement("select * from u_user ");
//			ResultSet rs = ps.executeQuery();
//			while (rs.next()) {
//				User user = new User();
//				user.setId(rs.getInt(1));
//				user.setU_name(rs.getString(2));
//				user.setU_age(rs.getInt(3));
//				user.setU_sex(rs.getString(4));
//				user.setU_xk(rs.getString(5));
//				user.setU_fdy(rs.getString(6));
//				user.setU_js(rs.getString(7));
//				
//				users.add(user);
//				// ?????????к??м???????????????????User????????????List??
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} finally {
//			close();
//		}
//		return users;
//	}
//
//	public User getUserById(Integer id) {
//		User user = null;
//		try {
//			prepareConnection();
//			ps = con.prepareStatement("select * from t_user where id=?");
//			ps.setInt(1, id);
//			ResultSet rs = ps.executeQuery();
//			if (rs.next()) {
//				user = new User();
//				user.setId(rs.getInt(1));
//				user.setU_name(rs.getString(2));
//				user.setU_age(rs.getInt(3));
//				user.setU_sex(rs.getString(4));
//				user.setU_xk(rs.getString(5));
//				user.setU_fdy(rs.getString(6));
//				user.setU_js(rs.getString(7));
//				// ?????????к??м???????????????????User????
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} finally {
//			close();
//		}
//		return user;
//	}

}
