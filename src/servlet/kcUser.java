package servlet;

import java.io.IOException;
import java.util.List;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.kUser;
import dao.sUser;
import dao.userDao;

public class kcUser extends HttpServlet {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public kcUser() {
		super();
	}


	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}


	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
                  doPost(request, response);

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		      request.setCharacterEncoding("utf-8");
             String cx = request.getParameter("cx").replace(" ", "");
        	 String a = request.getParameter("id");
			if(cx.equals("cx")){
				   request.getSession().setAttribute("moren2", "");
				   request.getSession().setAttribute("moren1", "active");
            	   queryAll(request, response);
             }else if(cx.equals("ck")){
            	 request.getSession().setAttribute("moren1", "");
            	 request.getSession().setAttribute("moren2", "active");
            	 Integer id = Integer.valueOf(a);
            	 getkUserById(request, response, id);
            	 queryAll(request, response);
             }else if(cx.equals("bc")){
            	 String id= request.getParameter("bcId");
            	 String word = (String) request.getSession().getAttribute("yh");
            	 sUser suser = new sUser();
            	 suser.setS_xk(Integer.parseInt(id));
            	 suser.setS_word(word);
            	 bcId(request, response, suser);
             }else if(cx.equals("zf")){
            	 request.getRequestDispatcher("/home.jsp").forward(request, response);
             } else{
            	 request.getRequestDispatcher("/error.jsp").forward(request, response);
             }
             
	}


	public void init() throws ServletException {
		// Put your code here
	}
	
	private void queryAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		List<kUser> kusers=new userDao().getAllkUser();
		request.getSession().setAttribute("kuse", kusers);
		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}
	
	private void getkUserById(HttpServletRequest request, HttpServletResponse response,Integer id) throws ServletException, IOException{
        kUser kuser = new userDao().getkUserById(id);
        request.getSession().setAttribute("kuser1", kuser);
	}
	private void bcId(HttpServletRequest request, HttpServletResponse response,sUser suser) throws ServletException, IOException{

        new userDao().bcId(suser);
		request.getRequestDispatcher("/win.jsp").forward(request, response);   
	}

}
