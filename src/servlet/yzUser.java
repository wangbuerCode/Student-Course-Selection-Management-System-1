package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.kUser;
import dao.sUser;
import dao.userDao;

public class yzUser extends HttpServlet {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public yzUser() {
		super();
	}


	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
             doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		    request.setCharacterEncoding("utf-8");
            String met = request.getParameter("dlzc");
            if(met.equals("dl")){
            	 dl(request, response);
            }else if(met.equals("zc")){
            	zc(request, response);
            }else{
            	 request.getRequestDispatcher("/error.jsp").forward(request, response);
            }

	}


	public void init() throws ServletException {
		// Put your code here
	}
	
	public void dl(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String word = request.getParameter("word");
        String pass = request.getParameter("pass");
        sUser suser = new sUser();
        suser.setS_word(word);
        suser.setS_pass(pass);
        if(new userDao().yzUser(suser)){
        	//cx查询
    		   request.getSession().setAttribute("yh", word);
    		   userDao userdao = new userDao();
    		   Integer a = userdao.getNameByid(suser);
		       if(a != 0){
			       kUser kuser = userdao.getIdByKc(a);
			       request.getSession().setAttribute("kuser1", kuser);
		       }
        	   request.getRequestDispatcher("kcUser?cx=cx").forward(request, response);
        }else{
        	   request.getRequestDispatcher("/index.jsp?end=end").forward(request, response);
        }
	}
	
    
	public void zc(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String word = request.getParameter("word");
		String name = request.getParameter("name");
        String pass = request.getParameter("pass");
        String bj   = request.getParameter("bj");
        String yx   = request.getParameter("yx");
        sUser suser = new sUser();
        suser.setS_name(name);
        suser.setS_pass(pass);
        suser.setS_word(word);
        suser.setS_bj(Integer.parseInt(bj));
        suser.setS_xy(yx);
        new userDao().zcUser(suser);
        request.getRequestDispatcher("MyJsp.jsp").forward(request, response);
}
	
}
