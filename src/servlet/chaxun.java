package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.kUser;
import dao.userDao;

public class chaxun extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	/**
	 * Constructor of the object.
	 */
	public chaxun() {
		super();
	}


	public void destroy() {
		super.destroy(); 
	}

	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
                  doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		 request.setCharacterEncoding("utf-8");
        String name = request.getParameter("name").replace(" ", "");
        kUser kuser = new userDao().getkUserByName(name);
        request.setAttribute("query", kuser);
    	request.getRequestDispatcher("query.jsp").forward(request, response);
	}

	
	public void init() throws ServletException {
           
	}

}
